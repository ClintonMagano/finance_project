package com.psybergate.finance.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.spi.CDI;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.interfaces.ForecastEntry;

@ViewScoped
@Named
public class InvestmentModel implements Serializable {

	private static final long serialVersionUID = 5411768203181211691L;

	private Money principal;

	private Integer term;

	private Money monthlyContribution;

	private Double interestRate;

	private Map<Integer, Event> events;

	private List<ForecastEntry> forecast;

	public InvestmentModel() {
		InvestmentData investmentData = CDI.current().select(InvestmentData.class).get();
		setPrincipal(investmentData.getPrincipal());
		setTerm(investmentData.getTerm());
		setMonthlyContribution(investmentData.getMonthlyContribution());
		setInterestRate(investmentData.getInterestRate());
		setEvents(investmentData.getEvents());
	}

	public void addEvent(Event event) {
		events.put(event.getMonth(), event);
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(Money monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Map<Integer, Event> getEvents() {
		return events;
	}

	public void setEvents(Map<Integer, Event> events) {
		this.events = events;
	}

	public List<ForecastEntry> getForecast() {
		return forecast;
	}

	public void setForecast(List<ForecastEntry> forecast) {
		this.forecast = forecast;
	}

	@Override
	public String toString() {
		return "InvestmentModel [principal=" + principal + ", term=" + term + ", monthlyContribution="
				+ monthlyContribution + ", interestRate=" + interestRate + ", events=" + events + ", forecast="
				+ forecast + "]";
	}

	public Money getClosingBalance() {
		if (forecast == null || forecast.isEmpty()) {
			return new Money();
		}
		return forecast.get(forecast.size() - 1).getClosingBalance();
	}

}