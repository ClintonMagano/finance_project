package com.psybergate.finance.model;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.psybergate.finance.interfaces.ForecastInfoViewModel;

@RequestScoped
@Named("forecastInfo")
public class ForecastInfoViewModelImpl implements ForecastInfoViewModel {

	private int customerNum;

	private String customerName;

	private String forecastName;

	public ForecastInfoViewModelImpl() {
	}

	public ForecastInfoViewModelImpl(int customerNum, String customerName, String forecastName) {
		this.customerNum = customerNum;
		this.customerName = customerName;
		this.forecastName = forecastName;
	}

	public ForecastInfoViewModelImpl(int customerNum, String investmentName) {
		this(customerNum, "", investmentName);
	}

	@Override
	public int getCustomerNum() {
		return customerNum;
	}

	@Override
	public void setCustomerNum(int customerNum) {
		this.customerNum = customerNum;
	}

	@Override
	public String getCustomerName() {
		return customerName;
	}

	@Override
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Override
	public String getForecastName() {
		return forecastName;
	}

	@Override
	public void setForecastName(String forecastName) {
		this.forecastName = forecastName;
	}

}
