package com.psybergate.finance.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;

@RequestScoped
@Named
public class PropertyBondData implements Serializable {

	private static final long serialVersionUID = 5411768203181211691L;

	private Money principal;

	private Integer term;

	private Double interestRate;

	Map<Integer, Event> events = new HashMap<>();

	public PropertyBondData() {
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Map<Integer, Event> getEvents() {
		return events;
	}

	public void setEvents(Map<Integer, Event> events) {
		this.events = events;
	}

	@Override
	public String toString() {
		return "PropertyBondData [principal=" + principal + ", term=" + term + ", interestRate=" + interestRate
				+ ", events=" + events + ", identity=" + System.identityHashCode(this) + "]";

	}

}