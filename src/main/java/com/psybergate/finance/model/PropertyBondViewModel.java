package com.psybergate.finance.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.spi.CDI;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.interfaces.ForecastEntry;

@ViewScoped
@Named
public class PropertyBondViewModel implements Serializable {

	private static final long serialVersionUID = -351474641909800194L;

	private String propertyBondName;

	private Money principal = new Money();

	private Integer term;

	private Double interestRate;

	private Money monthlyAmount = new Money();

	private List<ForecastEntry> forecast;

	private Map<Integer, Event> events = new HashMap<>();

	private Money conveyanceVatFees = new Money();

	private Money transferDutyFees = new Money();

	private Money conveyanceFees = new Money();

	private Money totalInterest = new Money();

	private Money totalRepayment = new Money();

	public PropertyBondViewModel() {
		PropertyBondData data = CDI.current().select(PropertyBondData.class).get();
		this.setPrincipal(data.getPrincipal());
		this.setTerm(data.getTerm());
		this.setInterestRate(data.getInterestRate());
		this.setEvents(data.getEvents());
	}

	public String getPropertyBondName() {
		return propertyBondName;
	}

	public void setPropertyBondName(String propertyBondName) {
		this.propertyBondName = propertyBondName;
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(Money monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public List<ForecastEntry> getForecast() {
		return forecast;
	}

	public void setForecast(List<ForecastEntry> forecast) {
		this.forecast = forecast;
	}

	public Map<Integer, Event> getEvents() {
		return events;
	}

	public void setEvents(Map<Integer, Event> events) {
		this.events = events;
	}

	public void addEvent(Integer month, Double interestRate, Money amount, Money monthlyAmount) {
		Event event = new Event(month, interestRate, amount, monthlyAmount);
		addEvent(event);
	}

	public void addEvent(EventModel eventModel) {
		addEvent(eventModel.getEvent());
	}

	public void addEvent(Event event) {
		events.put(event.getMonth(), event);
	}

	public PropertyBond generatePropertyBond() {
		return new PropertyBond(term, interestRate, principal, events);
	}

	public Money getTransferFees() {
		return conveyanceFees.add(conveyanceVatFees).add(transferDutyFees);
	}

	public void setConveyanceVatFees(Money conveyanceVatFees) {
		this.conveyanceVatFees = conveyanceVatFees;
	}

	public void setTransferDutyFees(Money transferDutyFees) {
		this.transferDutyFees = transferDutyFees;
	}

	public void setConveyanceFees(Money conveyanceFees) {
		this.conveyanceFees = conveyanceFees;
	}

	public Money getConveyanceVatFees() {
		return conveyanceVatFees;
	}

	public Money getTransferDutyFees() {
		return transferDutyFees;
	}

	public Money getConveyanceFees() {
		return conveyanceFees;
	}

	public Money getTotalInterest() {
		return totalInterest;
	}

	public void setTotalInterest(Money totalInterest) {
		this.totalInterest = totalInterest;
	}

	public Money getTotalRepayment() {
		return totalRepayment;
	}

	public void setTotalRepayment(Money totalRepayment) {
		this.totalRepayment = totalRepayment;
	}

	public Integer getMonthSettled() {
		Money closingBalance = getClosingBalance();
		if (closingBalance != null) {
			if (closingBalance.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
				return forecast.size();
			}
		}
		return null;
	}

	public Money getClosingBalance() {
		if (forecast == null || forecast.isEmpty()) return null;
		ForecastEntry lastEntry = forecast.get(forecast.size() - 1);
		return lastEntry.getClosingBalance();
	}
}
