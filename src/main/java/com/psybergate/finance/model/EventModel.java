package com.psybergate.finance.model;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;

@RequestScoped
@Named
public class EventModel {

	public static final String[] TRANSACTION_OPTIONS = { "Deposit", "Withdraw" };

	private Integer month;

	private Double interestRate;

	private Money monthlyAmount;

	private Money amount;

	private String transactionType;

	public EventModel() {
	}

	public Event getEvent() {
		Event event = new Event(month, interestRate, amount, monthlyAmount);
		if (amount != null && transactionType != null && transactionType.equalsIgnoreCase("withdraw")) {
			event.setAmount(new Money(amount.getAmount().negate()));
		}
		return event;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(Money monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public String[] getTransactionOptions() {
		return TRANSACTION_OPTIONS;
	}

	@Override
	public String toString() {
		return "EventModel [month=" + month + ", interestRate=" + interestRate + ", monthlyAmount=" + monthlyAmount
				+ ", amount=" + amount + ", transactionType=" + transactionType + "]";
	}
}
