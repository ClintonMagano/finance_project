package com.psybergate.finance.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;

@RequestScoped
@Named
public class InvestmentData implements Serializable {

	private static final long serialVersionUID = 5411768203181211691L;

	private Money principal;

	private Integer term;

	private Money monthlyContribution;

	private Double interestRate;

	private Map<Integer, Event> events;

	public InvestmentData() {
		events = new HashMap<Integer, Event>();
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(Money monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public void setEvents(Map<Integer, Event> events) {
		this.events = events;
	}

	public Map<Integer, Event> getEvents() {
		return events;
	}

	@Override
	public String toString() {
		return "InvestmentData [principal=" + principal + ", term=" + term + ", monthlyContribution=" + monthlyContribution
				+ ", interestRate=" + interestRate + ", id=" + System.identityHashCode(this) + " events:" + events.toString()
				+ "]";
	}

}