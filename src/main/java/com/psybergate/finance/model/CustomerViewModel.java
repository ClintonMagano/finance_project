package com.psybergate.finance.model;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@ApplicationScoped
@Named("customer")
public class CustomerViewModel {

	private String customerName;

	private Integer customerNum = 0;

	private String name;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Integer getCustomerNum() {
		return customerNum;
	}

	public void setCustomerNum(Integer customerNum) {
		this.customerNum = customerNum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return String.format("CustomerViewModel [customerName=%s, customerNum=%s, name=%s]", customerName, customerNum,
				name);
	}

}
