package com.psybergate.finance.exception;

public class ApplicationException extends RuntimeException {

	private static final long serialVersionUID = -8173813509633860432L;

	private ExceptionCode code;

	public ApplicationException(ExceptionCode code, String message) {
		this(message);
		setExceptionCode(code);
	}

	public ExceptionCode getExceptionCode() {
		return code;
	}

	public void setExceptionCode(ExceptionCode exceptionCode) {
		this.code = exceptionCode;
	}

	public ApplicationException() {
	}

	public ApplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ApplicationException(String message) {
		super(message);
	}

	public ApplicationException(Throwable cause) {
		super(cause);
	}
}
