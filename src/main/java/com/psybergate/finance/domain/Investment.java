package com.psybergate.finance.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.psybergate.finance.interfaces.ForecastEntry;

public class Investment {

	private Money principal;

	private Double interestRate;

	private Integer term;

	private Money monthlyContribution;

	private List<ForecastEntry> forecast;

	private Map<Integer, Event> events;

	public Investment() {
	}

	public Investment(Money principal, Double interestRate, Integer term, Money monthlyContribution,
			Map<Integer, Event> events) {
		setPrincipal(principal);
		setInterestRate(interestRate);
		setTerm(term);
		setMonthlyContribution(monthlyContribution);
		setEvents(events);
	}

	public void addForecastEntry(ForecastEntry entry) {
		forecast.add(entry);
	}

	public Money getFutureValue() {
		int index = forecast.size() - 1;
		return forecast.get(index).getClosingBalance();
	}

	public Money getTotalInterest() {
		Money totalInterest = getFutureValue().subtract(getPrincipal());
		return totalInterest;
	}

	public List<ForecastEntry> getForecast() {
		return forecast;
	}

	public Money getPrincipal() {
		return principal;
	}

	public void generateEntries() {
		setForecast(new ArrayList<>());
		ForecastEntry firstEntry =
				new InvestmentForecastEntry(1, getInterestRate(), getPrincipal(), getMonthlyContribution(), new Money());
		handleEvents(firstEntry);
		addForecastEntry(firstEntry);
		for (int month = 2; month <= getTerm(); month++) {
			final ForecastEntry prevEntry = getForecast().get(month - 2);
			ForecastEntry entry = prevEntry.generateNextEntry();
			handleEvents(entry);
			addForecastEntry(entry);
		}
	}

	private void handleEvents(ForecastEntry entry) {
		int month = entry.getMonth();
		if (getEvents().containsKey(month)) {
			Event event = getEvents().get(month);
			entry.applyEvent(event);
		}
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(Money monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	private void setForecast(List<ForecastEntry> forecast) {
		this.forecast = forecast;
	}

	public Map<Integer, Event> getEvents() {
		return events;
	}

	public void setEvents(Map<Integer, Event> events) {
		this.events = events;
		generateEntries();
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(String.format("Principal: R %s\n", getPrincipal()));
		stringBuilder.append(String.format("Term: %s\n", getTerm()));
		stringBuilder.append(String.format("Interest rate: %s\n", getInterestRate() + "%"));
		stringBuilder.append(String.format("Future value: R %s\n", getFutureValue()));
		stringBuilder.append(String.format("Forecast: %s\n", getForecast().toString()));

		return stringBuilder.toString();
	}
}
