package com.psybergate.finance.domain;

public class Event {

	private Integer month;

	private Double interestRate;

	/**
	 * The deposit or withdrawal amount. A positive value for a deposit, a negative value for
	 * withdrawal.
	 */
	private Money amount;

	private Money monthlyAmount;

	public Event() {
	}

	public Event(Integer month, Double interestRate, Money amount, Money monthlyAmount) {
		this.month = month;
		this.interestRate = interestRate;
		this.amount = amount;
		this.monthlyAmount = monthlyAmount;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public Money getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(Money monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public boolean interestRateChanged() {
		return interestRate != null;
	}

	public boolean monthlyAmountChanged() {
		return monthlyAmount != null;
	}

	public boolean additionalAmountChanged() {
		return amount != null;
	}

	@Override
	public String toString() {
		return "Event [month=" + month + ", interestRate=" + interestRate + ", amount=" + amount + ", monthlyAmount="
				+ monthlyAmount + "]";
	}

}