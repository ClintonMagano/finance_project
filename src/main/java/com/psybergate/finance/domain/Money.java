package com.psybergate.finance.domain;

import java.math.BigDecimal;

public class Money {

	private BigDecimal amount;

	public Money() {
		amount = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_CEILING);
	}

	public Money(BigDecimal amount) {
		this.amount = amount.setScale(2, BigDecimal.ROUND_CEILING);
	}

	public Money(double amount) {
		this(BigDecimal.valueOf(amount));
	}

	public Money(String amount) {
		this(new BigDecimal(amount));
	}

	public Money pow(int n) {
		return new Money(this.amount.pow(n));
	}

	public Money add(Money augend) {
		return new Money(this.getAmount().add(augend.getAmount()));
	}

	public Money add(double augend) {
		return new Money(amount.add(BigDecimal.valueOf(augend)));
	}

	public Money subtract(Money subtrahedren) {
		return new Money(this.getAmount().subtract(subtrahedren.getAmount()).setScale(2, BigDecimal.ROUND_CEILING));
	}

	public Money subtract(double subtrahedren) {
		return new Money(amount.subtract(BigDecimal.valueOf(subtrahedren)));
	}

	public Money multiply(Money multiplicand) {
		return new Money(this.getAmount().multiply(multiplicand.getAmount()));
	}

	public Money multiply(double multiplicand) {
		return new Money(amount.multiply(BigDecimal.valueOf(multiplicand)));
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount.setScale(2, BigDecimal.ROUND_CEILING);
	}

	public void setAmount(double amount) {
		setAmount(BigDecimal.valueOf(amount));
	}

	public int compareTo(Money money) {
		return this.getAmount().compareTo(money.getAmount());
	}

	public Money divide(double divisor) {
		return new Money(amount.divide(BigDecimal.valueOf(divisor)));
	}

	@Override
	public String toString() {
		return String.format("%s", amount);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Money other = (Money) obj;
		if (amount == null) {
			if (other.amount != null) return false;
		} else if (!amount.equals(other.amount)) {
			return false;
		}
		return true;
	}

}