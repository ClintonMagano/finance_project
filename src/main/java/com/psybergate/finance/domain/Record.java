package com.psybergate.finance.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Record {

	public static final String BOND_RECORD = "Property Bond";

	public static final String INVESTMENT_RECORD = "Investment";

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;

	private int customerNum;

	private String name;

	private String principal;

	private double interestRate;

	private int term;

	private String monthlyAmount;

	private String recordType;

	public Record() {
	}

	public Record(int customerNum, String investmentName, String principal, double interestRate, int term,
			String monthlyAmount, String recordType) {
		this.customerNum = customerNum;
		this.name = investmentName;
		this.principal = principal;
		this.interestRate = interestRate;
		this.term = term;
		this.monthlyAmount = monthlyAmount;
		this.recordType = recordType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String investmentName) {
		this.name = investmentName;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public String getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(String monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public int getCustomerNum() {
		return customerNum;
	}

	public void setCustomerNum(int customerNum) {
		this.customerNum = customerNum;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

}
