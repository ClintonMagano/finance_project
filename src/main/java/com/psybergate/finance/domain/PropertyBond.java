package com.psybergate.finance.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.psybergate.finance.interfaces.ForecastEntry;
import com.psybergate.finance.util.PropertyBondCalculatorUtil;

public class PropertyBond {

	private int term;

	private double interestRate;

	private Money principal;

	private List<ForecastEntry> forecast = new ArrayList<>();

	private Map<Integer, Event> events = new HashMap<>();

	private boolean paidUp;

	public boolean isPaidUp() {
		return paidUp;
	}

	private void setPaidUp(boolean paidUp) {
		this.paidUp = paidUp;
	}

	public PropertyBond(int term, double interestRate, Money principal, Map<Integer, Event> events) {
		setPrincipal(principal);
		setTerm(term);
		setInterestRate(interestRate);
		setEvents(events);
	}

	public Money getMonthlyRepayment() {
		return PropertyBondCalculatorUtil.getMonthlyRepayment(interestRate, term, principal);
	}

	public Money getTransferFees() {
		return getConveyanceFees().add(getTransferDutyFees()).add(getConveyanceVatFees());
	}

	public Money getConveyanceFees() {
		return PropertyBondCalculatorUtil.getConveyanceFees(principal);
	}

	public Money getTransferDutyFees() {
		return PropertyBondCalculatorUtil.getTransferDutyFees(principal);
	}

	public Money getConveyanceVatFees() {
		return PropertyBondCalculatorUtil.getConveyanceVatFees(principal);
	}

	public List<ForecastEntry> getForecast() {
		return forecast;
	}

	public int getTerm() {
		return term;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money openingBalance) {
		this.principal = openingBalance;
	}

	public Map<Integer, Event> getEvents() {
		return events;
	}

	public void setEvents(Map<Integer, Event> events) {
		this.events = events;
		generateEntries();
	}

	private void setForecast(List<ForecastEntry> forecast) {
		this.forecast = forecast;
	}

	public Money getClosingBalance() {
		return forecast.get(forecast.size() - 1).getClosingBalance();
	}

	public Money getTotalInterest() {
		Money totalRepayment = getTotalRepayment();
		return totalRepayment.subtract(principal);
	}

	public Money getTotalRepayment() {
		Money totalRepayment = new Money();
		for (ForecastEntry forecastEntry : forecast) {
			Money totalRepaymentForMonth = forecastEntry.getMonthlyAmount().add(forecastEntry.getAmount());
			totalRepayment = totalRepayment.add(totalRepaymentForMonth);
		}
		return totalRepayment;
	}

	private void addForecastEntry(ForecastEntry entry) {
		forecast.add(entry);
	}

	public void generateEntries() {
		setForecast(new ArrayList<>());
		ForecastEntry entry = new PropertyForecastEntry(1, interestRate, principal, getMonthlyRepayment(), new Money());
		applyEvents(entry);
		addForecastEntry(entry);
		for (int month = 2; month <= getTerm() && !paidUp; month++) {
			ForecastEntry prevEntry = getForecast().get(month - 2);
			entry = prevEntry.generateNextEntry();
			applyEvents(entry);
			paidUp = entry.getClosingBalance().getAmount().doubleValue() <= 0;
			addForecastEntry(entry);
		}
		clearBalance(entry);
	}

	private void clearBalance(ForecastEntry entry) {
		Money correction = entry.getMonthlyAmount().add(entry.getClosingBalance());
		entry.setMonthlyAmount(correction);
		setPaidUp(true);
	}

	public Integer getMonthPaidUp() {
		if (isPaidUp()) {
			return forecast.size();
		}
		return null;
	}

	private void applyEvents(ForecastEntry entry) {
		Event event = getEventForMonth(entry.getMonth());
		if (event != null) {
			Money newMonthlyRepayment =
					PropertyBondCalculatorUtil.getNewMonthlyRepayment(entry, event, term - entry.getMonth() + 1);
			Money currentMonthlyRepayment = entry.getMonthlyAmount();
			if (event.monthlyAmountChanged()) {
				currentMonthlyRepayment = event.getMonthlyAmount();
			}
			if (currentMonthlyRepayment.compareTo(newMonthlyRepayment) < 0) {
				event.setMonthlyAmount(newMonthlyRepayment);
			}
			entry.applyEvent(event);
		}
	}

	private Event getEventForMonth(int month) {
		return getEvents().get(month);
	}

	public void addEvent(Event event) {
		events.put(event.getMonth(), event);
		generateEntries();
	}
}
