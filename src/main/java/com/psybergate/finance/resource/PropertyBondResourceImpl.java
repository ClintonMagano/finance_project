package com.psybergate.finance.resource;

import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.psybergate.finance.interfaces.PropertyBondResource;
import com.psybergate.finance.resource.entity.EventRecord;
import com.psybergate.finance.resource.entity.PropertyBondRecord;

@RequestScoped
public class PropertyBondResourceImpl implements PropertyBondResource {

	@PersistenceContext(unitName = "financeDatabase")
	private EntityManager entityManager;

	@Override
	public void savePropertyBondRecord(PropertyBondRecord propertyBondRecord) {
		entityManager.persist(propertyBondRecord);
	}

	@Override
	public void savePropertyBondRecord(PropertyBondRecord propertyBondRecord, Map<Integer, EventRecord> eventRecords) {
		savePropertyBondRecord(propertyBondRecord);
		for (EventRecord record : eventRecords.values()) {
			record.setForecastId(propertyBondRecord.getId());
			entityManager.persist(record);
		}
	}

	@Override
	public List<PropertyBondRecord> getSavedPropertyBondRecords(Integer customerNum) {
		TypedQuery<PropertyBondRecord> query = entityManager.createQuery(
				"SELECT r FROM PropertyBondRecord AS r WHERE r.customer.customerNum = :customerNum", PropertyBondRecord.class);
		query.setParameter("customerNum", customerNum);
		List<PropertyBondRecord> resultList = query.getResultList();
		return resultList;
	}

	@Override
	public List<EventRecord> getPropertyBondEventRecords(Integer propertyBondId) {
		TypedQuery<EventRecord> query =
				entityManager.createQuery("SELECT e FROM EventRecord AS e WHERE e.forecastId = :forecastId", EventRecord.class);
		query.setParameter("forecastId", propertyBondId);
		return query.getResultList();
	}

}
