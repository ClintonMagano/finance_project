package com.psybergate.finance.resource;

import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.psybergate.finance.domain.Record;
import com.psybergate.finance.interfaces.ForecastResource;
import com.psybergate.finance.resource.entity.EventRecord;

@RequestScoped
public class ForecastResourceImpl implements ForecastResource {

	@PersistenceContext(unitName = "financeDatabase")
	private EntityManager entityManager;

	@Override
	public List<Record> getAvailableRecords(int customerNum) {
		TypedQuery<Record> query = entityManager.createQuery("SELECT r FROM Record AS r WHERE r.customerNum = :customerNum",
				Record.class);
		query.setParameter("customerNum", customerNum);
		List<Record> resultList = query.getResultList();
		return resultList;
	}

	@Override
	public void saveForecast(Record record) {
		entityManager.persist(record);
	}

	@Override
	public void saveEventRecords(Map<Integer, EventRecord> eventRecords) {
		for (EventRecord record : eventRecords.values()) {
			entityManager.persist(record);
		}
	}

	@Override
	public List<EventRecord> getForecastEventRecords(int forecastId) {
		TypedQuery<EventRecord> query = entityManager
				.createQuery("SELECT e FROM EventRecord AS e WHERE e.forecastId = :forecastId", EventRecord.class);
		query.setParameter("forecastId", forecastId);
		return query.getResultList();
	}
}
