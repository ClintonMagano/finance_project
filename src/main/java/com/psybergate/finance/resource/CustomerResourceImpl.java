package com.psybergate.finance.resource;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.interfaces.CustomerResource;

@RequestScoped
public class CustomerResourceImpl implements CustomerResource {

	@PersistenceContext(unitName = "financeDatabase")
	private EntityManager entityManager;

	@Override
	public void saveCustomer(Customer customer) {
		entityManager.persist(customer);
	}
}
