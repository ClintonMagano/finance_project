package com.psybergate.finance.resource;

import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.psybergate.finance.interfaces.InvestmentResource;
import com.psybergate.finance.resource.entity.EventRecord;
import com.psybergate.finance.resource.entity.InvestmentRecord;

@RequestScoped
public class InvestmentResourceImpl implements InvestmentResource {

	@PersistenceContext(unitName = "financeDatabase")
	private EntityManager entityManager;

	@Override
	public void saveInvestmentRecord(InvestmentRecord investmentRecord) {
		entityManager.persist(investmentRecord);
	}

	@Override
	public void saveInvestmentRecord(InvestmentRecord investmentRecord, Map<Integer, EventRecord> eventRecords) {
		saveInvestmentRecord(investmentRecord);
		for (EventRecord record : eventRecords.values()) {
			record.setInvestmentId(investmentRecord.getId());
			entityManager.persist(record);
		}
	}

	@Override
	public List<InvestmentRecord> getSavedInvestmentRecords(Integer customerNum) {
		TypedQuery<InvestmentRecord> query = entityManager.createQuery(
				"SELECT r FROM InvestmentRecord AS r WHERE r.customer.customerNum = :customerNum", InvestmentRecord.class);
		query.setParameter("customerNum", customerNum);
		List<InvestmentRecord> resultList = query.getResultList();
		return resultList;
	}

	@Override
	public List<EventRecord> getInvestmentEventRecords(Integer investmentId) {
		TypedQuery<EventRecord> query =
				entityManager.createQuery("SELECT e FROM EventRecord AS e WHERE e.forecastId = :forecastId", EventRecord.class);
		query.setParameter("forecastId", investmentId);
		return query.getResultList();
	}

}
