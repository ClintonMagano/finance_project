package com.psybergate.finance.resource.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.psybergate.finance.domain.Customer;

@Entity
public class InvestmentRecord {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;

	@ManyToOne
	private Customer customer;

	private String investmentName;

	private String principal;

	private Double interestRate;

	private Integer term;

	private String monthlyContribution;

	public InvestmentRecord() {
	}

	public InvestmentRecord(String investmentName, String principal, Double interestRate, Integer term,
			String monthlyContribution) {
		super();
		this.investmentName = investmentName;
		this.principal = principal;
		this.interestRate = interestRate;
		this.term = term;
		this.monthlyContribution = monthlyContribution;
	}

	public InvestmentRecord(Customer customer, String investmentName, String principal, Double interestRate, Integer term,
			String monthlyContribution) {
		super();
		this.customer = customer;
		this.investmentName = investmentName;
		this.principal = principal;
		this.interestRate = interestRate;
		this.term = term;
		this.monthlyContribution = monthlyContribution;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getInvestmentName() {
		return investmentName;
	}

	public void setInvestmentName(String investmentName) {
		this.investmentName = investmentName;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public String getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(String monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

}
