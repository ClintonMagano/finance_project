package com.psybergate.finance.resource.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.psybergate.finance.domain.Customer;

@Entity
public class PropertyBondRecord {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;

	@ManyToOne
	private Customer customer;

	private String propertyBondName;

	private String principal;

	private Double interestRate;

	private Integer term;

	public PropertyBondRecord() {
	}

	public PropertyBondRecord(Customer customer, String propertyBondName, String principal, Double interestRate,
			Integer term) {
		super();
		this.customer = customer;
		this.propertyBondName = propertyBondName;
		this.principal = principal;
		this.interestRate = interestRate;
		this.term = term;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getPropertyBondName() {
		return propertyBondName;
	}

	public void setPropertyBondName(String investmentName) {
		this.propertyBondName = investmentName;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

}
