package com.psybergate.finance.resource.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EventRecord {

	public static final String TYPE_INVESTMENT = "Investment";

	public static final String TYPE_PROPERTY_BOND = "Property Bond";

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Integer id;

	private Integer forecastId;

	private Integer month;

	private Double interestRate;

	private String amount;

	private String monthlyAmount;

	private String instrumentType;

	public EventRecord() {
	}

	public EventRecord(Integer month, Double interestRate, String amount, String monthlyAmount) {
		this.month = month;
		this.interestRate = interestRate;
		this.amount = amount;
		this.monthlyAmount = monthlyAmount;
	}

	public EventRecord(Integer month, Double interestRate, String amount, String monthlyAmount, String instrumentType) {
		super();
		this.month = month;
		this.interestRate = interestRate;
		this.amount = amount;
		this.monthlyAmount = monthlyAmount;
		this.instrumentType = instrumentType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getForecastId() {
		return forecastId;
	}

	public void setForecastId(Integer forecastId) {
		this.forecastId = forecastId;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(String monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}

	public Integer getInvestmentId() {
		return forecastId;
	}

	public void setInvestmentId(Integer investmentId) {
		this.forecastId = investmentId;
	}

	public String getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}

}
