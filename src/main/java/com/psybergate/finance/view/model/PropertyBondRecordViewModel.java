package com.psybergate.finance.view.model;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@RequestScoped
@Named
public class PropertyBondRecordViewModel {

	private Integer id;

	private Integer customerNum;

	private String propertyBondName;

	private String principal;

	private Double interestRate;

	private Integer term;

	public PropertyBondRecordViewModel(Integer id, Integer customerNum, String propertyBondName, String principal,
			Double interestRate, Integer term) {
		super();
		this.id = id;
		this.customerNum = customerNum;
		this.propertyBondName = propertyBondName;
		this.principal = principal;
		this.interestRate = interestRate;
		this.term = term;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerNum() {
		return customerNum;
	}

	public void setCustomerNum(Integer customerNum) {
		this.customerNum = customerNum;
	}

	public String getPropertyBondName() {
		return propertyBondName;
	}

	public void setPropertyBondName(String propertyBondName) {
		this.propertyBondName = propertyBondName;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

}
