package com.psybergate.finance.view.model;

import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.interfaces.ForecastEntry;

@RequestScoped
@Named
public class InvestmentViewModel {

	private String investmentName;

	private Money principal;

	private Integer term;

	private Double interestRate;

	private Money monthlyContribution;

	private Map<Integer, Event> events;

	private List<ForecastEntry> forecast;

	public String getInvestmentName() {
		return investmentName;
	}

	public void setInvestmentName(String investmentName) {
		this.investmentName = investmentName;
	}

	public Money getPrincipal() {
		return principal;
	}

	public void setPrincipal(Money principal) {
		this.principal = principal;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Money getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(Money monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

	public void addEvent(Event event) {
		events.put(event.getMonth(), event);
	}

	public Map<Integer, Event> getEvents() {
		return events;
	}

	public void setEvents(Map<Integer, Event> events) {
		this.events = events;
	}

	public List<ForecastEntry> getForecast() {
		return forecast;
	}

	public void setForecast(List<ForecastEntry> forecast) {
		this.forecast = forecast;
	}

	public Money getClosingBalance() {
		if (forecast == null || forecast.isEmpty()) {
			return new Money();
		}
		return forecast.get(forecast.size() - 1).getClosingBalance();
	}

	@Override
	public String toString() {
		return "InvestmentModel [principal=" + principal + ", term=" + term + ", monthlyContribution=" + monthlyContribution
				+ ", interestRate=" + interestRate + ", events=" + events + ", forecast=" + forecast + "]";
	}

}
