package com.psybergate.finance.view.model;

import javax.inject.Named;

@Named
public class InvestmentRecordViewModel {

	private Integer id;

	private Integer customerNum;

	private String investmentName;

	private String principal;

	private Double interestRate;

	private Integer term;

	private String monthlyContribution;

	public InvestmentRecordViewModel(Integer id, Integer customerNum, String investmentName, String principal,
			Double interestRate, Integer term, String monthlyContribution) {
		super();
		this.id = id;
		this.customerNum = customerNum;
		this.investmentName = investmentName;
		this.principal = principal;
		this.interestRate = interestRate;
		this.term = term;
		this.monthlyContribution = monthlyContribution;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerNum() {
		return customerNum;
	}

	public void setCustomerNum(Integer customerNum) {
		this.customerNum = customerNum;
	}

	public String getInvestmentName() {
		return investmentName;
	}

	public void setInvestmentName(String investmentName) {
		this.investmentName = investmentName;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public Double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(Double interestRate) {
		this.interestRate = interestRate;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public String getMonthlyContribution() {
		return monthlyContribution;
	}

	public void setMonthlyContribution(String monthlyContribution) {
		this.monthlyContribution = monthlyContribution;
	}

}
