package com.psybergate.finance.controller;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.psybergate.finance.controller.dispatcher.PageDispatcher;
import com.psybergate.finance.domain.Record;
import com.psybergate.finance.interfaces.ForecastInfoViewModel;
import com.psybergate.finance.interfaces.ForecastService;
import com.psybergate.finance.resource.entity.PropertyBondRecord;

@ApplicationScoped
@Named
public class ForecastController {

	@Inject
	private ForecastInfoViewModel forecastInfoViewModel;

	@Inject
	private ForecastService forecastService;

	@Inject
	private InvestmentController investmentController;

	@Inject
	private PropertyBondController propertyBondController;

	private PageDispatcher pageDispatcher = new PageDispatcher();

	public List<Record> getListOfForecasts() {
		int customerNum = forecastInfoViewModel.getCustomerNum();
		return forecastService.listAvailableForecasts(customerNum);
	}

	public String generateForecast(Record record) {
		String nextPage = "";
		if (record.getRecordType().equals(Record.INVESTMENT_RECORD)) {
//			investmentController.generateForecast((InvestmentRecord) record);
			nextPage = "investmentForecast";
		} else if (record.getRecordType().equals(Record.BOND_RECORD)) {
//			propertyBondController.generateForecast((PropertyBondRecord) record);
			nextPage = "propertyBondForecast";
		}

		return pageDispatcher.getPage(nextPage);
	}
}
