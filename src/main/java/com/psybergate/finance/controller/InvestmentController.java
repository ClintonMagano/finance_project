package com.psybergate.finance.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Named;

import org.bouncycastle.jce.PrincipalUtil;

import com.psybergate.finance.controller.dispatcher.PageDispatcher;
import com.psybergate.finance.controller.validator.InvestmentEventValidator;
import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.exception.ApplicationException;
import com.psybergate.finance.interfaces.CustomerService;
import com.psybergate.finance.interfaces.InvestmentService;
import com.psybergate.finance.model.CustomerViewModel;
import com.psybergate.finance.model.EventModel;
import com.psybergate.finance.model.InvestmentData;
import com.psybergate.finance.model.InvestmentModel;
import com.psybergate.finance.resource.entity.InvestmentRecord;
import com.psybergate.finance.util.PrintUtil;
import com.psybergate.finance.view.model.InvestmentRecordViewModel;
import com.psybergate.finance.view.model.InvestmentViewModel;
import com.psybergate.web.view.forecast.investment.InvestmentLineVisualisation;

@ApplicationScoped
@Named("investmentController")
public class InvestmentController {

	@Inject
	private CustomerViewModel customerViewModel;

	@Inject
	private InvestmentModel investmentModel;

	@Inject
	private InvestmentViewModel investmentViewModel;

	@Inject
	private InvestmentData investmentData;

	@Inject
	private InvestmentService investmentService;

	@Inject
	private CustomerService customerService;

	@Inject
	private EventModel eventModel;

	private PageDispatcher pageDispatcher = new PageDispatcher();

	public InvestmentController() {
	}

	public void generateForecast() {
		Investment investment = getInvestment();
		investmentModel.setEvents(investment.getEvents());
		investmentModel.setForecast(investment.getForecast());
		investmentModel.setInterestRate(investment.getInterestRate());
		investmentModel.setMonthlyContribution(investment.getMonthlyContribution());
		investmentModel.setPrincipal(investment.getPrincipal());
		investmentModel.setTerm(investment.getTerm());
	}

	public void generateForecast(InvestmentModel investmentModel) {
		Investment investment = investmentService.generateInvestment(investmentModel);
		investmentModel.setForecast(investment.getForecast());
	}

	public String generateForecastFromRecord(InvestmentRecordViewModel investmentRecordViewModel) {
		Money principal = new Money(investmentRecordViewModel.getPrincipal());
		Double interestRate = investmentRecordViewModel.getInterestRate();
		Money monthlyContribution = new Money(investmentRecordViewModel.getMonthlyContribution());
		Integer term = investmentRecordViewModel.getTerm();
		Integer investmentId = investmentRecordViewModel.getId();
		PrintUtil.print("investmentId: " + investmentId);
		Map<Integer, Event> events = investmentService.getInvestmentEvents(investmentId);

		PrintUtil.print("events: " + events);
		investmentData.setPrincipal(principal);
		investmentData.setInterestRate(interestRate);
		investmentData.setTerm(term);
		investmentData.setMonthlyContribution(monthlyContribution);
		investmentData.setEvents(events);

		getInvestment();
		return pageDispatcher.getPage("investmentForecast");
	}

	public void addEvent() {
		Event event = eventModel.getEvent();
		try {
			InvestmentEventValidator validator = CDI.current().select(InvestmentEventValidator.class).get();
			validator.validateEvent(event);
			investmentModel.addEvent(event);
		}
		catch (ApplicationException e) {}
	}

	public Investment getInvestment() {
		Investment investment;
		try {
			InvestmentEventValidator validator = new InvestmentEventValidator();
			validator.investmentData(investmentData);

			investmentModel.setPrincipal(investmentData.getPrincipal());
			investmentModel.setInterestRate(investmentData.getInterestRate());
			investmentModel.setTerm(investmentData.getTerm());
			investmentModel.setMonthlyContribution(investmentData.getMonthlyContribution());

			investment = investmentService.generateInvestment(investmentModel);
		}
		catch (ApplicationException e) {
			final Integer term = investmentModel.getTerm();
			Double interestRate = investmentModel.getInterestRate();
			Money principal = investmentModel.getPrincipal();
			Money monthlyContribution = investmentModel.getMonthlyContribution();
			Map<Integer, Event> events = investmentModel.getEvents();
			investment = new Investment(principal, interestRate, term, monthlyContribution, events);
		}
		return investment;
	}

	public String saveInvestment() {
		String customerName = customerViewModel.getName();
		Integer customerNum = customerViewModel.getCustomerNum();
		String investmentName = investmentViewModel.getInvestmentName();
		BigDecimal principal = investmentModel.getPrincipal().getAmount();
		Integer term = investmentModel.getTerm();
		BigDecimal monthlyContribution = investmentModel.getMonthlyContribution().getAmount();
		Double interestRate = investmentModel.getInterestRate();
		Map<Integer, Event> events = investmentModel.getEvents();

		Customer customer = new Customer(customerNum, customerName);
		customerService.saveCustomer(customer);
		investmentService.saveInvestment(customer, investmentName, principal, term, interestRate, monthlyContribution,
				events);

		return pageDispatcher.getPage("investmentForecast");
	}

	public List<InvestmentRecordViewModel> getListOfInvestments() {
		System.out.println(">>>>>>>>>>>>>>> customerViewModel: " + customerViewModel);

		Integer customerNum = customerViewModel.getCustomerNum();

		List<InvestmentRecord> investmentRecords = investmentService.getSavedInvestmentRecords(customerNum);

		List<InvestmentRecordViewModel> records = new ArrayList<InvestmentRecordViewModel>();
		for (InvestmentRecord investmentRecord : investmentRecords) {
			Integer id = investmentRecord.getId();
			String investmentName = investmentRecord.getInvestmentName();
			String principal = investmentRecord.getPrincipal();
			Double interestRate = investmentRecord.getInterestRate();
			Integer term = investmentRecord.getTerm();

			InvestmentRecordViewModel record = new InvestmentRecordViewModel(id, customerNum, investmentName, principal,
					interestRate, term, investmentRecord.getMonthlyContribution());
			records.add(record);
			System.out.println("<<<<<<<<<<<<");
		}

		return records;
	}

	public static String someMethod(InvestmentModel investmentModel,
			InvestmentLineVisualisation investmentLineVisualisation) {
		String result = "".concat(investmentModel.getForecast().get(investmentModel.getForecast().size() - 1)
				.getClosingBalance().getAmount().subtract(investmentModel.getPrincipal().getAmount())
				.divide(investmentLineVisualisation.getPrincipalTermAsBigDecimal(investmentModel), 2, RoundingMode.HALF_UP)
				.toString());
		return result;
	}
}
