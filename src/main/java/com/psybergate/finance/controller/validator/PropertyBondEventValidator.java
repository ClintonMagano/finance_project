package com.psybergate.finance.controller.validator;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.exception.ApplicationException;
import com.psybergate.finance.interfaces.ForecastEntry;
import com.psybergate.finance.model.EventModel;
import com.psybergate.finance.model.PropertyBondData;
import com.psybergate.finance.model.PropertyBondViewModel;
import com.psybergate.finance.util.PropertyBondCalculatorUtil;

@RequestScoped
@Named
public class PropertyBondEventValidator implements Serializable {

	private static final long serialVersionUID = 4213302635411428745L;

	private static final String FORM_NAME = "propertyBondEventForm";

	public static final Money MINIMUM_PRINCIPAL_FOR_PROPERTY_BOND = new Money(1);

	public static final int MINIMUM_TERM_FOR_PROPERTY_BOND = 1;

	public static final int MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND = 1;

	public static final int MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND = 100;

	public static final Money MINIMUM_MONTHLY_REPAYMENT_FOR_PROPERTY_BOND = new Money(0);

	private Map<String, String> errorMessages = new HashMap<>();

	private boolean valid = true;

	public void validateEvent() throws ApplicationException {
		EventModel currentEventModel = getCurrentEventModel();
		validateEvent(currentEventModel.getEvent());
	}

	public void validateEvent(Event event) throws ApplicationException {
		try {
			validateMonth(event);
		}
		catch (ApplicationException e) {
			setValid(false);
			errorMessages.put("month", e.getMessage());
		}
		try {
			validateInterestRate(event);
		}
		catch (ApplicationException e) {
			setValid(false);
			errorMessages.put("interestRate", e.getMessage());
		}
		try {
			validateAmount(event);
		}
		catch (ApplicationException e) {
			setValid(false);
			errorMessages.put("amount", e.getMessage());
		}
		try {
			validateMonthlyRepayment(event);
		}
		catch (ApplicationException e) {
			setValid(false);
			errorMessages.put("monthlyAmount", e.getMessage());
		}

		if (errorMessages.isEmpty()) {
			if (!(event.additionalAmountChanged() || event.interestRateChanged() || event.monthlyAmountChanged())) {
				String message = "you have not specified any changes for this month ";
				errorMessages.put("event", message);
				setValid(false);
			}
		}
		if (!errorMessages.isEmpty()) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			for (Entry<String, String> entry : errorMessages.entrySet()) {
				facesContext.addMessage(FORM_NAME + ":" + entry.getKey(), getMessage(entry.getValue()));
			}
			facesContext.renderResponse();
			throw new ApplicationException("Event validation failed");
		}
	}

	private void validateMonth(Event event) {
		Integer month = event.getMonth();
		PropertyBondViewModel propertyBondViewModel = getCurrentPropertyBondViewModel();
		Integer maxMonth = propertyBondViewModel.getMonthSettled();
		if (maxMonth == null) {
			maxMonth = propertyBondViewModel.getTerm();
		}
		if (month == null || month < 1 || month > maxMonth) {
			String message = String.format("month must be between %s and %s (inclusive)", 1, maxMonth);
			throw new ApplicationException(message);
		}
	}

	private void validateMonthlyRepayment(Event event) {
		if (errorMessages.containsKey("month")) {
			throw new ApplicationException("this value depends on month, please specify a valid month first");
		}
		if (errorMessages.containsKey("interestRate")) {
			throw new ApplicationException("this value depends on interest rate, please specify a valid interest rate first");
		}
		if (!event.monthlyAmountChanged()) return;
		PropertyBondViewModel propertyBondViewModel = getCurrentPropertyBondViewModel();
		ForecastEntry entry = propertyBondViewModel.getForecast().get(event.getMonth() - 1);
		int newTerm = propertyBondViewModel.getTerm() - event.getMonth() + 1;
		Money newMonthlyRepayment = PropertyBondCalculatorUtil.getNewMonthlyRepayment(entry, event, newTerm);

		Money monthlyRepayment = event.getMonthlyAmount();
		if (monthlyRepayment.compareTo(newMonthlyRepayment) < 0) {
			String message = String.format("monthly contribution should not be less than %s", newMonthlyRepayment);
			throw new ApplicationException(message);
		}
	}

	private void validateAmount(Event event) {
		if (!event.additionalAmountChanged()) return;
		if (errorMessages.containsKey("month")) {
			throw new ApplicationException("this value depends on month, please specify a valid month first");
		}
		int month = event.getMonth();
		PropertyBondViewModel propertyBondViewModel = getCurrentPropertyBondViewModel();
		Money amount = event.getAmount();
		Money openingBalance = propertyBondViewModel.getForecast().get(month - 1).getOpeningBalance();
		if (openingBalance.add(amount).getAmount().doubleValue() < 0) {
			String message = String.format("maximum withdrawal amount is %s", openingBalance);
			throw new ApplicationException(message);
		}
	}

	private void validateInterestRate(Event event) {
		if (!event.interestRateChanged()) return;
		double interestRate = event.getInterestRate();
		if (interestRate < MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND
				|| interestRate > MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND) {
			String message = String.format("Interest rate should be in range (%s, %s)",
					MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND, MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND);
			throw new ApplicationException(message);
		}
	}

	public boolean isValid() {
		return valid;

	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void validatePropertyBondData(PropertyBondData propertyBondData) throws ApplicationException {
		if (propertyBondData == null) throw new ApplicationException("data is null");
		validatePrincipal(propertyBondData);
		validateTerm(propertyBondData);
		validateInterestRate(propertyBondData);
	}

	private void validateInterestRate(PropertyBondData propertyBondData) {
		if (propertyBondData.getInterestRate() == null)
			throw new ApplicationException("interest rate not specified in data");
	}

	private void validateTerm(PropertyBondData propertyBondData) {
		if (propertyBondData.getTerm() == null) throw new ApplicationException("term not specified in data");
	}

	private void validatePrincipal(PropertyBondData propertyBondData) {
		if (propertyBondData.getPrincipal() == null) throw new ApplicationException("principal not specified in data");
	}

	public FacesMessage getMessage(String message) {
		FacesMessage facesMessage = new FacesMessage("", message);
		facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
		return facesMessage;
	}

	private EventModel getCurrentEventModel() {
		return CDI.current().select(EventModel.class).get();
	}

	private PropertyBondViewModel getCurrentPropertyBondViewModel() {
		return CDI.current().select(PropertyBondViewModel.class).get();
	}
}
