package com.psybergate.finance.controller.validator;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

import com.psybergate.finance.domain.Money;
import com.psybergate.finance.exception.ApplicationException;
import com.psybergate.finance.interfaces.InvestmentValidatorService;

@ApplicationScoped
@Named
public class InvestmentValidator implements InvestmentValidatorService, Serializable {

	private static final long serialVersionUID = 4213302635411428745L;

	public static final Money MINIMUM_PRINCIPAL_FOR_INVESTMENT = new Money(1);

	public static final int MINIMUM_TERM_FOR_INVESTMENT = 1;

	public static final int MINIMUM_INTEREST_RATE_FOR_INVESTMENT = 1;

	public static final int MAXIMUM_INTEREST_RATE_FOR_INVESTMENT = 100;

	public static final Money MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT = new Money(0);

	public void validatePrincipal(FacesContext context, UIComponent component, Object principalObject) {
		try {
			validatePrincipal((Money) principalObject);
		}
		catch (ApplicationException e) {
			handleException(e);
		}
	}

	public void validateTerm(FacesContext context, UIComponent component, Object termObject) {
		try {
			validateTerm((Integer) termObject);
		}
		catch (ApplicationException e) {
			handleException(e);
		}
	}

	public void validateInterestRate(FacesContext context, UIComponent component, Object interestRateObject) {
		try {
			validateInterestRate((Double) interestRateObject);
		}
		catch (ApplicationException e) {
			handleException(e);
		}
	}

	public void validateMonthlyContribution(FacesContext context, UIComponent component,
			Object monthlyContributionObject) {
		if (monthlyContributionObject == null) {
			return;
		}
		try {
			validateMonthlyContribution((Money) monthlyContributionObject);
		}
		catch (ApplicationException e) {
			handleException(e);
		}
	}

	public void validatePrincipal(Money principal) throws ApplicationException {
		if (principal == null) {
			throw new ValidatorException(getMessage("principal should not be empty"));
		}
		if (principal.compareTo(MINIMUM_PRINCIPAL_FOR_INVESTMENT) < 0) {
			String message = String.format("principal should not be less than %s", MINIMUM_PRINCIPAL_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	public void validateTerm(Integer term) {
		if (term == null) {
			throw new ApplicationException("term should not be empty");
		}
		if (term < MINIMUM_TERM_FOR_INVESTMENT) {
			String message = String.format("term should not be less than %s", MINIMUM_TERM_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	private void validateInterestRate(Double interestRate) {
		if (interestRate == null) {
			throw new ApplicationException("interest rate should not be empty");
		}
		if (interestRate < MINIMUM_INTEREST_RATE_FOR_INVESTMENT || interestRate > MAXIMUM_INTEREST_RATE_FOR_INVESTMENT) {
			String message = String.format("Interest rate should be in range (%s, %s)", MINIMUM_INTEREST_RATE_FOR_INVESTMENT,
					MAXIMUM_INTEREST_RATE_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	private void validateMonthlyContribution(Money monthlyContribution) {
		if (monthlyContribution == null) {
			throw new ApplicationException("monthly contribution should not be empty");
		}
		if (monthlyContribution.compareTo(MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT) < 0) {
			String message =
					String.format("monthly contribution should not be less than %s", MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	private void handleException(ApplicationException e) {
		throw new ValidatorException(getMessage(e.getMessage()));
	}

	public FacesMessage getMessage(String message) {
		FacesMessage facesMessage = new FacesMessage("", message);
		facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
		return facesMessage;
	}

}
