package com.psybergate.finance.controller.validator;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.exception.ApplicationException;
import com.psybergate.finance.interfaces.InvestmentValidatorService;
import com.psybergate.finance.model.InvestmentData;
import com.psybergate.finance.model.InvestmentModel;

@RequestScoped
@Named
public class InvestmentEventValidator implements InvestmentValidatorService, Serializable {

	public static final String FORM_NAME = "investmentEventForm";

	private static final long serialVersionUID = 4213302635411428745L;

	public static final Money MINIMUM_PRINCIPAL_FOR_INVESTMENT = new Money(1);

	public static final Integer MINIMUM_TERM_FOR_INVESTMENT = 1;

	public static final Integer MINIMUM_INTEREST_RATE_FOR_INVESTMENT = 1;

	public static final Integer MAXIMUM_INTEREST_RATE_FOR_INVESTMENT = 100;

	public static final Money MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT = new Money(0);

	private boolean valid = true;

	private Map<String, String> errorMessages = new HashMap<>();

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public void validateEvent(Event event) throws ApplicationException {
		try {
			validateMonth(event);
		}
		catch (ApplicationException e) {
			setValid(false);
			errorMessages.put("month", e.getMessage());
		}
		try {
			validateMonthlyContribution(event);
		}
		catch (ApplicationException e) {
			setValid(false);
			errorMessages.put("monthlyAmount", e.getMessage());
		}
		try {
			validateAmount(event);
		}
		catch (ApplicationException e) {
			setValid(false);
			errorMessages.put("amount", e.getMessage());
		}
		try {
			validateInterestRate(event);
		}
		catch (ApplicationException e) {
			setValid(false);
			errorMessages.put("interestRate", e.getMessage());
		}

		if (errorMessages.isEmpty()) {
			if (!(event.additionalAmountChanged() || event.interestRateChanged() || event.monthlyAmountChanged())) {
				String message = "you have not specified any changes for this month ";
				errorMessages.put("event", message);
				setValid(false);
			}
		}
		if (!errorMessages.isEmpty()) {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			for (Entry<String, String> entry : errorMessages.entrySet()) {
				facesContext.addMessage(FORM_NAME + ":" + entry.getKey(), getMessage(entry.getValue()));
			}
			facesContext.renderResponse();
			throw new ApplicationException("Event validation failed");
		}
	}

	private void validateMonth(Event event) {
		InvestmentModel investmentModel = getCurrentInvestmentModel();
		Integer maxMonth = investmentModel.getTerm();
		Integer month = event.getMonth();
		if (month == null || month < 1 || month > maxMonth) {
			throw new ApplicationException(String.format("month must be in range (%s, %s)", 1, maxMonth));
		}
	}

	private void validateInterestRate(Event event) {
		if (!event.interestRateChanged()) return;
		Double interestRate = event.getInterestRate();
		if (interestRate == null || interestRate < MINIMUM_INTEREST_RATE_FOR_INVESTMENT
				|| interestRate > MAXIMUM_INTEREST_RATE_FOR_INVESTMENT) {
			String message = String.format("Interest rate should be in range (%s, %s)", MINIMUM_INTEREST_RATE_FOR_INVESTMENT,
					MAXIMUM_INTEREST_RATE_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	private void validateAmount(Event event) {
		if (!event.additionalAmountChanged()) return;
		Integer month = event.getMonth();
		Money amount = event.getAmount();
		if (errorMessages.containsKey("month")) {
			throw new ApplicationException("this value depends on month, please provide valid month");
		}
		if (amount.getAmount().doubleValue() < 0) {
			InvestmentModel investmentModel = getCurrentInvestmentModel();
			Money maxWithdrawable = investmentModel.getForecast().get(month - 1).getOpeningBalance();
			if (amount.add(maxWithdrawable).getAmount().doubleValue() < 0) {
				String message = String.format("maximum withdrawal amount is %s", maxWithdrawable);
				throw new ApplicationException(message);
			}

		}
	}

	private void validateMonthlyContribution(Event event) {
		if (!event.monthlyAmountChanged()) return;
		Money monthlyContribution = event.getMonthlyAmount();
		if (monthlyContribution == null
				|| (monthlyContribution.compareTo(MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT) < 0)) {
			String message =
					String.format("monthly contribution should not be less than %s", MINIMUM_MONTHLY_CONTRIBUTION_FOR_INVESTMENT);
			throw new ApplicationException(message);
		}
	}

	public static FacesMessage getMessage(String message) {
		FacesMessage facesMessage = new FacesMessage("", message);
		facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
		return facesMessage;
	}

	private InvestmentModel getCurrentInvestmentModel() {
		return CDI.current().select(InvestmentModel.class).get();
	}

	public void investmentData(InvestmentData investmentData) {
		if (investmentData == null) throw new ApplicationException("data is null");
		if (investmentData.getPrincipal() == null) throw new ApplicationException("principal not specified in data");
		if (investmentData.getTerm() == null) throw new ApplicationException("term not specified in data");
		if (investmentData.getInterestRate() == null) throw new ApplicationException("interest rate not specified in data");
	}

}
