package com.psybergate.finance.controller.validator;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import com.psybergate.finance.domain.Money;
import com.psybergate.finance.interfaces.InvestmentValidatorService;

@ViewScoped
@Named
public class PropertyBondValidator implements InvestmentValidatorService, Serializable {

	private static final long serialVersionUID = 4213302635411428745L;

	public static final Money MINIMUM_PRINCIPAL_FOR_PROPERTY_BOND = new Money(1);

	public static final int MINIMUM_TERM_FOR_PROPERTY_BOND = 1;

	public static final int MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND = 1;

	public static final int MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND = 100;

	public static final Money MINIMUM_MONTHLY_CONTRIBUTION_FOR_PROPERTY_BOND = new Money(0);

	public void validatePrincipal(FacesContext context, UIComponent component, Object principalObject) {
		if (principalObject == null) {
			throw new ValidatorException(getMessage("principal should not be empty"));
		}
		Money principal = (Money) principalObject;
		if (principal.compareTo(MINIMUM_PRINCIPAL_FOR_PROPERTY_BOND) < 0) {
			String message = String.format("principal should not be less than %s", MINIMUM_PRINCIPAL_FOR_PROPERTY_BOND);
			throw new ValidatorException(getMessage(message));
		}
	}

	public void validateMonthlyContribution(FacesContext context, UIComponent component,
			Object monthlyContributionObject) {
		if (monthlyContributionObject == null) {
			return;
		}
		Money monthlyContribution = (Money) monthlyContributionObject;
		if (monthlyContribution.compareTo(MINIMUM_MONTHLY_CONTRIBUTION_FOR_PROPERTY_BOND) < 0) {
			String message = String.format("monthly contribution should not be less than %s",
					MINIMUM_MONTHLY_CONTRIBUTION_FOR_PROPERTY_BOND);
			throw new ValidatorException(getMessage(message));
		}

	}

	public void validateTerm(FacesContext context, UIComponent component, Object termObject) {
		if (termObject == null) {
			throw new ValidatorException(getMessage("term should not be empty"));
		}
		int term = (int) termObject;
		if (term < MINIMUM_TERM_FOR_PROPERTY_BOND) {
			String message = String.format("term should not be less than %s", MINIMUM_TERM_FOR_PROPERTY_BOND);
			throw new ValidatorException(getMessage(message));
		}
	}

	public void validateInterestRate(FacesContext context, UIComponent component, Object interestRateObject) {
		if (interestRateObject == null) {
			throw new ValidatorException(getMessage("interest should not be empty"));
		}
		double interestRate = (double) interestRateObject;
		if (interestRate < MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND
				|| interestRate > MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND) {
			String message = String.format("Interest rate should be in range (%s, %s)",
					MINIMUM_INTEREST_RATE_FOR_PROPERTY_BOND, MAXIMUM_INTEREST_RATE_FOR_PROPERTY_BOND);
			throw new ValidatorException(getMessage(message));
		}
	}

	public static FacesMessage getMessage(String message) {
		FacesMessage facesMessage = new FacesMessage("", message);
		facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
		return facesMessage;
	}

}
