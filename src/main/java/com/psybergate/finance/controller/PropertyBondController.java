package com.psybergate.finance.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.CDI;
import javax.inject.Inject;
import javax.inject.Named;

import com.psybergate.finance.controller.dispatcher.PageDispatcher;
import com.psybergate.finance.controller.validator.PropertyBondEventValidator;
import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.exception.ApplicationException;
import com.psybergate.finance.interfaces.CustomerService;
import com.psybergate.finance.interfaces.PropertyBondService;
import com.psybergate.finance.model.CustomerViewModel;
import com.psybergate.finance.model.EventModel;
import com.psybergate.finance.model.PropertyBondData;
import com.psybergate.finance.model.PropertyBondViewModel;
import com.psybergate.finance.resource.entity.PropertyBondRecord;
import com.psybergate.finance.view.model.PropertyBondRecordViewModel;
import com.psybergate.web.view.forecast.propertybond.PropertyBondLineVisualisation;

@ApplicationScoped
@Named
public class PropertyBondController {

	@Inject
	private CustomerViewModel customerViewModel;

	@Inject
	private PropertyBondViewModel propertyBondViewModel;

	@Inject
	private PropertyBondService propertyBondService;

	@Inject
	private PropertyBondData propertyBondData;

	@Inject
	private CustomerService customerService;

	private PageDispatcher pageDispatcher = new PageDispatcher();

	@Inject
	private EventModel eventModel;

	public void generateForecast() {
		PropertyBond propertyBond = getPropertyBond();
		propertyBondViewModel.setForecast(propertyBond.getForecast());
		propertyBondViewModel.setPrincipal(propertyBond.getPrincipal());
		propertyBondViewModel.setConveyanceFees(propertyBond.getConveyanceFees());
		propertyBondViewModel.setConveyanceVatFees(propertyBond.getConveyanceVatFees());
		propertyBondViewModel.setTransferDutyFees(propertyBond.getTransferDutyFees());
		propertyBondViewModel.setTotalInterest(propertyBond.getTotalInterest());
		propertyBondViewModel.setTotalRepayment(propertyBond.getTotalRepayment());
	}

	public String generateForecastFromRecord(PropertyBondRecordViewModel propertyBondRecordViewModel) {
		System.out.println("=============");
		Money principal = new Money(propertyBondRecordViewModel.getPrincipal());
		Double interestRate = propertyBondRecordViewModel.getInterestRate();
		Integer term = propertyBondRecordViewModel.getTerm();
		Map<Integer, Event> events = propertyBondService.getPropertyBondEvents(propertyBondRecordViewModel.getId());

		propertyBondData.setPrincipal(principal);
		propertyBondData.setInterestRate(interestRate);
		propertyBondData.setTerm(term);
		propertyBondData.setEvents(events);

		System.out.println("######### " + propertyBondData);
		getPropertyBond();
		return pageDispatcher.getPage("propertyBondForecast");
	}

	public PropertyBond getPropertyBond() {
		PropertyBond propertyBond;
		try {
			PropertyBondEventValidator validator = new PropertyBondEventValidator();
			validator.validatePropertyBondData(propertyBondData);
			propertyBond = propertyBondService.generatePopertyBond(propertyBondData);
		}
		catch (ApplicationException e) {
			final Integer term = propertyBondViewModel.getTerm();
			Double interestRate = propertyBondViewModel.getInterestRate();
			Money principal = propertyBondViewModel.getPrincipal();
			Map<Integer, Event> events = propertyBondViewModel.getEvents();
			propertyBond = new PropertyBond(term, interestRate, principal, events);
		}
		return propertyBond;
	}

	public Event getEvent(EventModel eventModel) {
		Integer month = eventModel.getMonth();
		Double interestRate = eventModel.getInterestRate();
		Money amount = eventModel.getAmount();
		Money monthlyAmount = eventModel.getMonthlyAmount();
		String transactionType = eventModel.getTransactionType();
		if (amount != null && transactionType != null && transactionType.equalsIgnoreCase("withdraw")) {
			amount = new Money(amount.getAmount().negate());
		}
		return new Event(month, interestRate, amount, monthlyAmount);
	}

	public String generateForecast(PropertyBondRecord propertyBondRecord) {
		Double interestRate = propertyBondRecord.getInterestRate();
		String principal = propertyBondRecord.getPrincipal();
		Integer term = propertyBondRecord.getTerm();
		Map<Integer, Event> events = propertyBondService.getPropertyBondEvents(propertyBondRecord.getId());

		propertyBondData.setInterestRate(interestRate);
		propertyBondData.setPrincipal(new Money(principal));
		propertyBondData.setTerm(term);
		propertyBondData.setEvents(events);

		return pageDispatcher.getPage("propertyBondForecast");
	}

	public void addEvent() {
		Event event = eventModel.getEvent();
		try {
			PropertyBondEventValidator validator = CDI.current().select(PropertyBondEventValidator.class).get();
			validator.validateEvent(event);
			propertyBondViewModel.addEvent(event);
		}
		catch (Exception e) {}
	}

	public String savePropertyBond() {
		String customerName = customerViewModel.getName();
		Integer customerNum = customerViewModel.getCustomerNum();
		String propertyBondName = propertyBondViewModel.getPropertyBondName();
		BigDecimal principal = propertyBondViewModel.getPrincipal().getAmount();
		Integer term = propertyBondViewModel.getTerm();
		Double interestRate = propertyBondViewModel.getInterestRate();
		Map<Integer, Event> events = propertyBondViewModel.getEvents();

		Customer customer = new Customer(customerNum, customerName);
		customerService.saveCustomer(customer);
		propertyBondService.savePropertyBond(customer, propertyBondName, principal, term, interestRate, events);

		return pageDispatcher.getPage("propertyBondForecast");
	}

	public List<PropertyBondRecordViewModel> getListOfPropertyBonds() {
		System.out.println("Property Bond >>>>>>>>>>>>>>> customerViewModel: " + customerViewModel);

		Integer customerNum = customerViewModel.getCustomerNum();

		List<PropertyBondRecord> propertyBondRecords = propertyBondService.getSavedPropertyBondRecords(customerNum);

		List<PropertyBondRecordViewModel> records = new ArrayList<PropertyBondRecordViewModel>();
		for (PropertyBondRecord propertyBondRecord : propertyBondRecords) {
			// This id will be used to retrieve events.
			Integer id = propertyBondRecord.getId();
			String propertyBondName = propertyBondRecord.getPropertyBondName();
			String principal = propertyBondRecord.getPrincipal();
			Double interestRate = propertyBondRecord.getInterestRate();
			Integer term = propertyBondRecord.getTerm();

			PropertyBondRecordViewModel record =
					new PropertyBondRecordViewModel(id, customerNum, propertyBondName, principal, interestRate, term);
			records.add(record);
			System.out.println("Property Bond <<<<<<<<<<<<");
		}

		return records;
	}

	public static BigDecimal someMethod(PropertyBondViewModel propertyBondViewModel,
			PropertyBondLineVisualisation propertyBondLineVisualisation) {
		// PrintUtil.print("first param: " + propertyBondViewModel);
		// PrintUtil.print("second param: " + propertyBondLineVisualisation);
		// BigDecimal someValue = propertyBondViewModel.getTotalRepayment().getAmount()
		// .subtract(propertyBondViewModel.getTotalRepayment().getAmount().divide(
		// propertyBondLineVisualisation.getPrincipalTermAsBigDecimal(propertyBondViewModel),
		// 2,
		// RoundingMode.HALF_UP))
		// .divide(propertyBondViewModel.getTotalRepayment().getAmount().divide(
		// propertyBondLineVisualisation.getPrincipalTermAsBigDecimal(propertyBondViewModel),
		// 2,
		// RoundingMode.HALF_UP));
		// return someValue;
		return new BigDecimal(2500.05).setScale(2, BigDecimal.ROUND_CEILING);
	}

}
