package com.psybergate.finance.interfaces;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.model.PropertyBondData;
import com.psybergate.finance.model.PropertyBondViewModel;
import com.psybergate.finance.resource.entity.PropertyBondRecord;

public interface PropertyBondService {

	public PropertyBond generatePopertyBond(PropertyBondData propertyBondData);

	public List<PropertyBondRecord> getSavedPropertyBondRecords(Integer customerNum);

	public Map<Integer, Event> getPropertyBondEvents(Integer id);

	public void savePropertyBond(Customer customer, String propertyBondName, BigDecimal principal, Integer term,
			Double interestRate, Map<Integer, Event> events);

}
