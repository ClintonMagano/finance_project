package com.psybergate.finance.interfaces;

import com.psybergate.finance.domain.Customer;

public interface CustomerResource {

	void saveCustomer(Customer customer);

}
