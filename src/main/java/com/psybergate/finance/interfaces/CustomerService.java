package com.psybergate.finance.interfaces;

import com.psybergate.finance.domain.Customer;

public interface CustomerService {

	public void saveCustomer(Customer customer);
	
	public void saveCustomer(Integer customerNum, String customerName);

}
