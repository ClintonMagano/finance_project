package com.psybergate.finance.interfaces;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;

public interface ForecastEntry extends Comparable<ForecastEntry> {

	public Money getClosingBalance();

	public Money getInterest();

	public Money getOpeningBalance();

	public double getInterestRate();

	public Money getMonthlyAmount();

	public int getMonth();

	public Money getAmount();

	public ForecastEntry generateNextEntry();

	public void setInterestRate(double interestRate);

	public void setMonthlyAmount(Money monthlyAmount);

	public void setAmount(Money amount);

	public void applyEvent(Event event);
}
