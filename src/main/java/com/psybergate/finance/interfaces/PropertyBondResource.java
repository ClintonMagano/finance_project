package com.psybergate.finance.interfaces;

import java.util.List;
import java.util.Map;

import com.psybergate.finance.resource.entity.EventRecord;
import com.psybergate.finance.resource.entity.PropertyBondRecord;

public interface PropertyBondResource {

	public void savePropertyBondRecord(PropertyBondRecord record);

	public void savePropertyBondRecord(PropertyBondRecord propertyBondRecord, Map<Integer, EventRecord> eventRecords);

	public List<PropertyBondRecord> getSavedPropertyBondRecords(Integer customerNum);

	public List<EventRecord> getPropertyBondEventRecords(Integer propertyBondId);

}
