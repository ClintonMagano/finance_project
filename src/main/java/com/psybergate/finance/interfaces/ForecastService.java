package com.psybergate.finance.interfaces;

import java.util.List;

import com.psybergate.finance.domain.Record;

public interface ForecastService {
	public List<Record> listAvailableForecasts(int customerNum);
}
