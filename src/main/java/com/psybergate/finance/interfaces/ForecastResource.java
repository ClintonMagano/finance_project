package com.psybergate.finance.interfaces;

import java.util.List;
import java.util.Map;

import com.psybergate.finance.domain.Record;
import com.psybergate.finance.resource.entity.EventRecord;

public interface ForecastResource {

	public List<Record> getAvailableRecords(int customerNum);

	public void saveForecast(Record record);

	public void saveEventRecords(Map<Integer, EventRecord> eventRecords);

	public List<EventRecord> getForecastEventRecords(int forecastId);

}
