package com.psybergate.finance.interfaces;

public interface ForecastInfoViewModel {

	public int getCustomerNum();

	public void setCustomerNum(int customerNum);

	public String getCustomerName();

	public void setCustomerName(String customerName);

	public String getForecastName();

	public void setForecastName(String forecastName);

}
