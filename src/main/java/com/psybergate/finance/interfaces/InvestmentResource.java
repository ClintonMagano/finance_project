package com.psybergate.finance.interfaces;

import java.util.List;
import java.util.Map;

import com.psybergate.finance.resource.entity.EventRecord;
import com.psybergate.finance.resource.entity.InvestmentRecord;

public interface InvestmentResource {

	public void saveInvestmentRecord(InvestmentRecord record);

	public void saveInvestmentRecord(InvestmentRecord investmentRecord, Map<Integer, EventRecord> eventRecords);

	public List<InvestmentRecord> getSavedInvestmentRecords(Integer customerNum);

	public List<EventRecord> getInvestmentEventRecords(Integer investmentId);

}
