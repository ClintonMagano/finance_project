package com.psybergate.finance.interfaces;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.model.InvestmentModel;
import com.psybergate.finance.resource.entity.InvestmentRecord;

public interface InvestmentService {

	public Investment generateInvestment(InvestmentModel investmentModel);

	public List<InvestmentRecord> getSavedInvestmentRecords(Integer customerNum);

	public Map<Integer, Event> getInvestmentEvents(Integer id);

	public void saveInvestment(Customer customer, String investmentName, BigDecimal principal, Integer term,
			Double interestRate, BigDecimal monthlyContribution, Map<Integer, Event> events);

}
