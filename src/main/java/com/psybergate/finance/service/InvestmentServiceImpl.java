package com.psybergate.finance.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Investment;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.interfaces.ForecastResource;
import com.psybergate.finance.interfaces.InvestmentResource;
import com.psybergate.finance.interfaces.InvestmentService;
import com.psybergate.finance.model.InvestmentModel;
import com.psybergate.finance.resource.entity.EventRecord;
import com.psybergate.finance.resource.entity.InvestmentRecord;

@ApplicationScoped
public class InvestmentServiceImpl implements InvestmentService {

	@Inject
	private InvestmentResource investmentResource;

	@Override
	public Investment generateInvestment(InvestmentModel investmentModel) {
		Investment investment = extractInvestment(investmentModel);
		investmentModel.setForecast(investment.getForecast());
		return investment;
	}

	private Investment extractInvestment(InvestmentModel investmentModel) {
		Money principal = investmentModel.getPrincipal();
		Double interestRate = investmentModel.getInterestRate();
		Integer term = investmentModel.getTerm();
		Money monthlyContribution = investmentModel.getMonthlyContribution();
		Map<Integer, Event> events = investmentModel.getEvents();
		return new Investment(principal, interestRate, term, monthlyContribution, events);
	}

	@Override
	@Transactional
	public void saveInvestment(Customer customer, String investmentName, BigDecimal principal, Integer term,
			Double interestRate, BigDecimal monthlyContribution, Map<Integer, Event> events) {
		// Validation happens here. Validate inputs, validate events.
		// Once validation passes, proceed with persistence.
		String principalValue = principal.toPlainString();
		String monthlyContributionValue = monthlyContribution.toPlainString();
		Map<Integer, EventRecord> eventRecords = extractEventRecords(events);

		InvestmentRecord record =
				new InvestmentRecord(customer, investmentName, principalValue, interestRate, term, monthlyContributionValue);

		investmentResource.saveInvestmentRecord(record, eventRecords);
	}

	@Override
	public List<InvestmentRecord> getSavedInvestmentRecords(Integer customerNum) {
		return investmentResource.getSavedInvestmentRecords(customerNum);
	}

	private Map<Integer, EventRecord> extractEventRecords(Map<Integer, Event> events) {
		Map<Integer, EventRecord> records = new HashMap<Integer, EventRecord>();
		for (Event event : events.values()) {
			Integer month = event.getMonth();
			Double interestRate = null;
			String monthlyAmount = null;
			String amount = null;
			String instrumentType = EventRecord.TYPE_INVESTMENT;

			if (event.interestRateChanged()) {
				interestRate = event.getInterestRate();
			}

			if (event.additionalAmountChanged()) {
				amount = event.getAmount().getAmount().toString();
			}

			if (event.monthlyAmountChanged()) {
				monthlyAmount = event.getMonthlyAmount().getAmount().toString();
			}

			EventRecord record = new EventRecord(month, interestRate, amount, monthlyAmount, instrumentType);
			records.put(month, record);
		}

		return records;
	}

	@Override
	public Map<Integer, Event> getInvestmentEvents(Integer investmentId) {
		List<EventRecord> eventRecords = investmentResource.getInvestmentEventRecords(investmentId);
		Map<Integer, Event> events = new HashMap<Integer, Event>();
		for (EventRecord eventRecord : eventRecords) {
			events.put(eventRecord.getMonth(), extractEvent(eventRecord));
		}

		return events;
	}

	private Event extractEvent(EventRecord eventRecord) {
		Integer month = eventRecord.getMonth();
		Double interestRate = eventRecord.getInterestRate();
		Money amount = null;
		if (eventRecord.getAmount() != null) {
			amount = new Money(eventRecord.getAmount());
		}
		Money monthlyAmount = null;
		if (eventRecord.getMonthlyAmount() != null) {
			monthlyAmount = new Money(eventRecord.getMonthlyAmount());
		}
		return new Event(month, interestRate, amount, monthlyAmount);
	}
}