package com.psybergate.finance.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.psybergate.finance.domain.Record;
import com.psybergate.finance.interfaces.ForecastResource;
import com.psybergate.finance.interfaces.ForecastService;

@ApplicationScoped
public class ForecastServiceImpl implements ForecastService {

	@Inject
	private ForecastResource forecastResource;

	@Override
	public List<Record> listAvailableForecasts(int customerNum) {
		return forecastResource.getAvailableRecords(customerNum);
	}

}
