package com.psybergate.finance.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.PropertyBond;
import com.psybergate.finance.interfaces.PropertyBondResource;
import com.psybergate.finance.interfaces.PropertyBondService;
import com.psybergate.finance.model.PropertyBondData;
import com.psybergate.finance.resource.entity.EventRecord;
import com.psybergate.finance.resource.entity.PropertyBondRecord;

@ApplicationScoped
public class PropertyBondServiceImpl implements PropertyBondService {

	@Inject
	private PropertyBondResource propertyBondResource;

	@Override
	public PropertyBond generatePopertyBond(PropertyBondData propertyBondData) {
		PropertyBond bond = new PropertyBond(propertyBondData.getTerm(), propertyBondData.getInterestRate(),
				propertyBondData.getPrincipal(), propertyBondData.getEvents());
		return bond;
	}

	@Override
	@Transactional
	public void savePropertyBond(Customer customer, String propertyBondName, BigDecimal principal, Integer term,
			Double interestRate, Map<Integer, Event> events) {
		// Validation happens here. Validate inputs, validate events.
		// Once validation passes, proceed with persistence.

		String principalValue = principal.toPlainString();
		Map<Integer, EventRecord> eventRecords = extractEventRecords(events);
		PropertyBondRecord record = new PropertyBondRecord(customer, propertyBondName, principalValue, interestRate, term);

		propertyBondResource.savePropertyBondRecord(record, eventRecords);
	}

	@Override
	public List<PropertyBondRecord> getSavedPropertyBondRecords(Integer customerNum) {
		return propertyBondResource.getSavedPropertyBondRecords(customerNum);
	}

	@Override
	public Map<Integer, Event> getPropertyBondEvents(Integer propertyBondId) {
		List<EventRecord> eventRecords = propertyBondResource.getPropertyBondEventRecords(propertyBondId);
		Map<Integer, Event> events = new HashMap<Integer, Event>();
		for (EventRecord eventRecord : eventRecords) {
			events.put(eventRecord.getMonth(), extractEvent(eventRecord));
		}

		return events;
	}

	private Event extractEvent(EventRecord eventRecord) {
		Integer month = eventRecord.getMonth();
		Double interestRate = eventRecord.getInterestRate();
		Money amount = null;
		if (eventRecord.getAmount() != null) {
			amount = new Money(eventRecord.getAmount());
		}
		Money monthlyAmount = null;
		if (eventRecord.getMonthlyAmount() != null) {
			monthlyAmount = new Money(eventRecord.getMonthlyAmount());
		}
		return new Event(month, interestRate, amount, monthlyAmount);
	}

	private Map<Integer, EventRecord> extractEventRecords(Map<Integer, Event> events) {
		Map<Integer, EventRecord> records = new HashMap<Integer, EventRecord>();
		for (Event event : events.values()) {
			Integer month = event.getMonth();
			Double interestRate = null;
			String monthlyAmount = null;
			String amount = null;
			String instrumentType = EventRecord.TYPE_PROPERTY_BOND;

			if (event.interestRateChanged()) {
				interestRate = event.getInterestRate();
			}

			if (event.additionalAmountChanged()) {
				amount = event.getAmount().getAmount().toString();
			}

			if (event.monthlyAmountChanged()) {
				monthlyAmount = event.getMonthlyAmount().getAmount().toString();
			}

			EventRecord record = new EventRecord(month, interestRate, amount, monthlyAmount, instrumentType);
			records.put(month, record);
		}

		return records;
	}

}
