package com.psybergate.finance.service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.psybergate.finance.domain.Customer;
import com.psybergate.finance.interfaces.CustomerService;
import com.psybergate.finance.model.CustomerViewModel;
import com.psybergate.finance.resource.CustomerResourceImpl;

@RequestScoped
public class CustomerServiceImpl implements CustomerService {

	@Inject
	private CustomerResourceImpl customerResource;

	@Override
	@Transactional
	public void saveCustomer(Integer customerNum, String customerName) {
		Customer customer = new Customer(customerNum, customerName);
		customerResource.saveCustomer(customer);
	}

	@Override
	@Transactional
	public void saveCustomer(Customer customer) {
		customerResource.saveCustomer(customer);
	}
}
