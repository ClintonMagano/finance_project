package com.psybergate.web.view.forecast.investment;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

import com.psybergate.finance.interfaces.ForecastEntry;
import com.psybergate.finance.model.InvestmentModel;

@RequestScoped
@Named
public class InvestmentLineVisualisation {

	public BigDecimal getPrincipalTermAsBigDecimal(InvestmentModel investmentModel) {
		return BigDecimal.valueOf(investmentModel.getTerm());
	}

	public LineChartModel getModel(InvestmentModel investmentModel) {
		LineChartModel model = new LineChartModel();

		LineChartSeries capitalGainPoints = new LineChartSeries();

		for (ForecastEntry forecastEntry : investmentModel.getForecast()) {
			capitalGainPoints.set(forecastEntry.getMonth(), forecastEntry.getClosingBalance().getAmount());
		}

		model.getAxis(AxisType.Y).setMin(-1);
		model.getAxis(AxisType.Y).setMax(investmentModel.getForecast().get(investmentModel.getForecast().size() - 1)
				.getClosingBalance().getAmount().add(getAverageOpeningBalances(investmentModel)));
		model.getAxis(AxisType.Y).setLabel("Principal (R)");

		model.getAxis(AxisType.X).setMin(0);
		model.getAxis(AxisType.X).setLabel("Month");

		model.addSeries(capitalGainPoints);
		model.setTitle("Principal Growth Over Time");
		return model;
	}

	private BigDecimal getAverageOpeningBalances(InvestmentModel investmentModel) {
		BigDecimal average = BigDecimal.ZERO;

		for (ForecastEntry forecastEntry : investmentModel.getForecast()) {
			average = average.add(forecastEntry.getClosingBalance().getAmount());
		}

		return average.divide(BigDecimal.valueOf(investmentModel.getTerm()), 2, RoundingMode.HALF_UP);
	}

}
